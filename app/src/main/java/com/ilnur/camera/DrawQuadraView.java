package com.ilnur.camera;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

public class DrawQuadraView extends View {

    private int mHeight;
    private int mWidth;
    private Point mPointA, mPointB, mPointC, mPointD, mPointX;
    private Random mRandom = new Random();

    public DrawQuadraView(Context context) {
        super(context);
    }

    public void makeInvalidate() {
        mPointA = new Point(getLeftCoordinate(), getTopCoordinate());
        mPointB = new Point(getRightCoordinate(), getTopCoordinate());
        mPointC = new Point(getRightCoordinate(), getBottomCoordinate());
        mPointD = new Point(getLeftCoordinate(), getBottomCoordinate());
        invalidate();
    }

    public ArrayList<Point> getPoints() {
        ArrayList<Point> points = new ArrayList<>();
        points.add(mPointA);
        points.add(mPointB);
        points.add(mPointC);
        points.add(mPointD);
        points.add(mPointX);
        return points;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mHeight = canvas.getHeight();
        mWidth = canvas.getWidth();

        mPointA = new Point(getLeftCoordinate(), getTopCoordinate());
        mPointB = new Point(getRightCoordinate(), getTopCoordinate());
        mPointC = new Point(getRightCoordinate(), getBottomCoordinate());
        mPointD = new Point(getLeftCoordinate(), getBottomCoordinate());
        mPointX = new Point(mWidth, mHeight);
        int alpha = 50;
        int red = 54, green = 218, blue = 54;
        int ar = Color.argb(alpha, red, green, blue);

        Paint canvasPaint = new Paint();
        canvasPaint.setColor(ar);
        canvasPaint.setStyle(Paint.Style.FILL);

        Path canvasPath = new Path();
        canvasPath.setFillType(Path.FillType.EVEN_ODD);
        canvasPath.moveTo(0, 0);
        canvasPath.lineTo(mWidth, 0);
        canvasPath.lineTo(mWidth, mHeight);
        canvasPath.lineTo(0, mHeight);
        canvasPath.lineTo(0, 0);
        canvasPath.lineTo(mPointA.x, mPointA.y);
        canvasPath.lineTo(mPointB.x, mPointB.y);
        canvasPath.lineTo(mPointC.x, mPointC.y);
        canvasPath.lineTo(mPointD.x, mPointD.y);
        canvasPath.lineTo(mPointA.x, mPointA.y);
        canvasPath.close();
        canvas.drawPath(canvasPath, canvasPaint);

        Paint quadraPaint = new Paint();
        quadraPaint.setStrokeWidth(4);
        quadraPaint.setColor(Color.RED);
        quadraPaint.setStyle(Paint.Style.STROKE);
        quadraPaint.setAntiAlias(true);

        Path quadraPath = new Path();
        quadraPath.setFillType(Path.FillType.EVEN_ODD);
        quadraPath.moveTo(mPointA.x, mPointA.y);
        quadraPath.lineTo(mPointB.x, mPointB.y);
        quadraPath.lineTo(mPointC.x, mPointC.y);
        quadraPath.lineTo(mPointD.x, mPointD.y);
        quadraPath.lineTo(mPointA.x, mPointA.y);
        quadraPath.close();
        canvas.drawPath(quadraPath, quadraPaint);
    }

    private int getLeftCoordinate() {
        return mRandom.nextInt((int) (mWidth / 2));
    }
    private int getRightCoordinate() {
        return mRandom.nextInt((int) (mWidth / 2)) + (int) (mWidth / 2);
    }
    private int getTopCoordinate() {
        return mRandom.nextInt((int) (mHeight / 2));
    }
    private int getBottomCoordinate() {
        return mRandom.nextInt((int) (mHeight / 2)) + (int) (mHeight / 2);
    }

}