package com.ilnur.camera.event;

import android.os.Handler;
import android.os.Looper;

import java.util.HashSet;

public class AppEventsHelper {
    private static AppEventsHelper instance = null;

    private HashSet<Listener> mListeners = new HashSet<>();

    private AppEventsHelper() { }

    public static AppEventsHelper getInstance() {
        if (instance == null) {
            instance = new AppEventsHelper();
        }

        return instance;
    }

    public void subscribe(Listener listener)
    {
        mListeners.add(listener);
    }

    public void unsubscribe(Listener listener)
    {
        mListeners.remove(listener);
    }

    public void fire() {
//    public void fire(final Bitmap data) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    for (Listener listener : mListeners) {
                        listener.OnTime();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
