package com.ilnur.camera.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.ilnur.camera.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShowResultActivity extends AppCompatActivity {
    @Bind(R.id.show_result_image_view)
    ImageView mShowImage;
    @Bind(R.id.accept_btn)
    FloatingActionButton mAccepBtn;
    @Bind(R.id.cancel_btn)
    FloatingActionButton mCancelBtn;

    private ArrayList<Point> mPoints;
    private Point mPointA, mPointB, mPointC, mPointD, mPointX;
    private int mHeight, mWidth;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        File deleteFile = new File(Environment.getExternalStorageDirectory() + "/camtest/temp.jpg");
        if(deleteFile.exists()) {
            deleteFile.delete();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        ButterKnife.bind(this);

        mPoints = getIntent().getParcelableArrayListExtra("path");

        try {
            FileInputStream fileInputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/camtest/temp.jpg");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable=true;
            Bitmap tempBitmap = BitmapFactory.decodeStream(fileInputStream);
            Bitmap bitmap = tempBitmap.copy(Bitmap.Config.ARGB_8888, true );

            Canvas canvas = new Canvas(bitmap);

            mHeight = canvas.getHeight();
            mWidth = canvas.getWidth();

            mPointA = mPoints.get(0);
            mPointB = mPoints.get(1);
            mPointC = mPoints.get(2);
            mPointD = mPoints.get(3);
            mPointX = mPoints.get(4);
            int proportionsHeigth = mHeight/ mPointX.y;
            int proportionsWidth = mWidth/ mPointX.x;

            int alpha = 50;
            int red = 54, green = 218, blue = 54;
            int ar = Color.argb(alpha, red, green, blue);

            Paint canvasPaint = new Paint();
            canvasPaint.setColor(ar);
            canvasPaint.setStyle(Paint.Style.FILL);

            Path canvasPath = new Path();
            canvasPath.setFillType(Path.FillType.EVEN_ODD);
            canvasPath.moveTo(0, 0);
            canvasPath.lineTo(mWidth, 0);
            canvasPath.lineTo(mWidth, mHeight);
            canvasPath.lineTo(0, mHeight);
            canvasPath.lineTo(0, 0);
            canvasPath.lineTo(mPointA.x * proportionsWidth, mPointA.y * proportionsHeigth);
            canvasPath.lineTo(mPointB.x * proportionsWidth, mPointB.y * proportionsHeigth);
            canvasPath.lineTo(mPointC.x * proportionsWidth, mPointC.y * proportionsHeigth);
            canvasPath.lineTo(mPointD.x * proportionsWidth, mPointD.y * proportionsHeigth);
            canvasPath.lineTo(mPointA.x * proportionsWidth, mPointA.y * proportionsHeigth);
            canvasPath.close();
            canvas.drawPath(canvasPath, canvasPaint);

            Paint quadraPaint = new Paint();
            quadraPaint.setStrokeWidth(8);
            quadraPaint.setColor(Color.RED);
            quadraPaint.setStyle(Paint.Style.STROKE);
            quadraPaint.setAntiAlias(true);

            Path quadraPath = new Path();
            quadraPath.setFillType(Path.FillType.EVEN_ODD);
            quadraPath.moveTo(mPointA.x * proportionsWidth, mPointA.y * proportionsHeigth);
            quadraPath.lineTo(mPointB.x * proportionsWidth, mPointB.y * proportionsHeigth);
            quadraPath.lineTo(mPointC.x * proportionsWidth, mPointC.y * proportionsHeigth);
            quadraPath.lineTo(mPointD.x * proportionsWidth, mPointD.y * proportionsHeigth);
            quadraPath.lineTo(mPointA.x * proportionsWidth, mPointA.y * proportionsHeigth);
            quadraPath.close();
            canvas.drawPath(quadraPath, quadraPaint);


            mShowImage.setImageBitmap(bitmap);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/camtest/temp.jpg")));
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mAccepBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File fileFrom = new File(Environment.getExternalStorageDirectory() + "/camtest/temp.jpg");
                File fileTo = new File(Environment.getExternalStorageDirectory() + "/camtest/"+String.format("%d.jpg", System.currentTimeMillis()));
                fileFrom.renameTo(fileTo);
                Intent intent = new Intent(ShowResultActivity.this, PreviewActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File deleteFile = new File(Environment.getExternalStorageDirectory() + "/camtest/temp.jpg");
                deleteFile.delete();
                Intent intent = new Intent(ShowResultActivity.this, PreviewActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
