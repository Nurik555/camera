package com.ilnur.camera.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ilnur.camera.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class BlackActivity extends AppCompatActivity {
    @Bind(R.id.camera_btn)
    FloatingActionButton mScanBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black);
        ButterKnife.bind(this);

        mScanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BlackActivity.this, PreviewActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
