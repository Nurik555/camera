package com.ilnur.camera.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ilnur.camera.CameraView;
import com.ilnur.camera.DrawQuadraView;
import com.ilnur.camera.R;
import com.ilnur.camera.event.AppEventsHelper;
import com.ilnur.camera.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PreviewActivity extends AppCompatActivity implements Listener {
    @Bind(R.id.relative_layout)
    RelativeLayout mRelativeLayout;

//    @Bind(R.id.toggle_camera_btn)
//    FloatingActionButton mToggleCameraChangeBtn;

    @Bind(R.id.camera_take_btn)
    FloatingActionButton mCameraTakeBtn;

    @Bind(R.id.camera_back_btn)
    FloatingActionButton mCameraBackBtn;

    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private Camera mCamera = null;
    private CameraView mCameraView = null;
    private DrawQuadraView mGameView = null;
    private int mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private ArrayList<Point> mPoints = new ArrayList<>();
    private Timer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "На этом устройстве не камеры.", Toast.LENGTH_LONG).show();
        } else {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PreviewActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            toggleCamera();
        }

        mCameraTakeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraView.takePicture();
                mTimer.cancel();
                mPoints = mGameView.getPoints();
            }
        });

//        mToggleCameraChangeBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
//                    mToggleCameraChangeBtn.setImageResource(R.drawable.ic_camera_front_white_48dp);
//                    mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
//                    mRotateCamera = true;
//                } else {
//                    mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
//                    mToggleCameraChangeBtn.setImageResource(R.drawable.ic_camera_rear_white_48dp);
//                }
//                toggleCamera();
//            }
//        });

        mCameraBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreviewActivity.this, BlackActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void toggleCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mRelativeLayout.removeView(mGameView);
            mRelativeLayout.removeView(mCameraView);
        }
        try {
            mCamera = Camera.open(mCameraId);
        } catch (Exception e) {
            Log.e("ERROR", "Failed to get camera: " + e.getMessage());
        }
        Camera.Parameters params = mCamera.getParameters();
        List<String> focusModes = params.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            params.setRotation(90);
            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            Camera.Size size = sizes.get(0);
            params.setPictureSize(size.width, size.height);
            params.setPictureFormat(ImageFormat.JPEG);
            mCamera.setParameters(params);
        }
        mCameraView = new CameraView(this, mCamera);
        mRelativeLayout.addView(mCameraView);
        drawRect();
    }

    private void drawRect() {
        mGameView = new DrawQuadraView(this);
        mRelativeLayout.addView(mGameView);

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(runn1);
            }
        }, 1000, 2000);
    }

    Runnable runn1 = new Runnable() {
        public void run() {
            mGameView.makeInvalidate();
        }
    };


    @Override
    public void OnTime() {
        Intent intent = new Intent(PreviewActivity.this, ShowResultActivity.class);
        intent.putExtra("path", mPoints);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsHelper.getInstance().subscribe(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsHelper.getInstance().unsubscribe(this);
    }
}

